package client.clientpushfiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v1.DbxClientV1;
import com.dropbox.core.v1.DbxWriteMode;
import com.dropbox.core.v2.DbxFiles.UploadException;

/**
 *  
 *	Upload folder content to dropbox account
 */
public class App 
{
	
	private static final String APP_ACCESS_TOKEN = "Aw2-mYuyOIAAAAAAAAAADi-DE4KoTAj7ZGIEJJxhfeYhCQdW_zcNsYo6hVd_3IYI";
	
	public static void main(String[] args) throws UploadException, DbxException, IOException {
    	
        System.out.println("Start client!");
        DbxRequestConfig config = new DbxRequestConfig("filesClient", Locale.getDefault().toString());
        
        File inputFile = new File("resources/test.txt");
        FileInputStream inputStream = new FileInputStream(inputFile);
        
        DbxClientV1 client = new DbxClientV1(config, APP_ACCESS_TOKEN);
        
        try {
        	com.dropbox.core.v1.DbxEntry.File uploadedFile = 
            		client.uploadFile("/test2.txt", DbxWriteMode.add(), inputFile.length(), inputStream);
        	System.out.println("Uploaded " + uploadedFile.toString());
        } finally {
        	inputStream.close();
        }
    }
}
